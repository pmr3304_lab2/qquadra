clube_data = [{
    "id":
    "1",
    "name":
    "Clube Hebraica",
    "release_year":
    "1994",
    "poster_url":
    "https://i.imgur.com/KwdIChv.jpg",
    "descrição":
    "Também localizado no bairro de Pinheiros, A Hebraica é um dos clubes de SP que tem atividades culturais e sociais além das esportivas. Aos 64 anos de idade, o clube conta hoje com oito quadras de tênis, quatro piscinas e seis quadras poliesportivas.A Hebraica tem dois teatros, um de 500 e outro de 250 lugares, e uma galeria de arte. Os sócios têm à disposição uma biblioteca e atendimento online exclusivo.Para tornar-se sócio, é preciso retirar um termo de reserva na recepção do clube, preencher e colher as assinaturas devidas. Após devolver na secretaria-geral, é necessário aguardar ser chamado para entregar a documentação necessária e efetuar os pagamentos. Um título do clube gira em torno de R$ 70 mil, segundo o jornal Folha de S.Paulo."
    
}, {
    "id":
    "2",
    "name":
    "Clube Paulistano",
    "release_year":
    "1972",
    "poster_url":
    "https://i.imgur.com/6SzVpUj.jpg",
    "descrição":
    'O Clube Atletico Paulistano também faz parte da lista de tradicionais clubes de SP. Fundado em 1900, o clube reúne atividades sociais, culturais e esportivas em seus mais de 41 mil m² no bairro do Jardim América. De acordo com o próprio clube, são em média 40 modalidades esportivas, cinema, teatro, shows, exposições, cursos e workshops. O ginásio foi projetado pelo arquiteto Paulo Mendes da Rocha.Faça chuva ou faça sol, este é um clube com piscina em SP para passar o dia. O complexo aquático foi projetado na década de 50 e conta com piscinas olímpica, semiolímpica, infantil e também coberta.'
}, {
    "id":
    "2",
    "name":
    "Clube Paineiras",
    "release_year":
    "1972",
    "poster_url":
    "https://i.imgur.com/pLFYhBZ.jpg",
    "descrição":
    'Fundado em 1960, o Clube Paineiras Morumby está localizado em um dos bairros mais nobres da cidade de São Paulo, o Morumbi. Seus 120 mil m² abrigam uma reserva de mata nativa, um vasto complexo esportivo e diversas opções de atividades sociais. Considerado um oásis de tranquilidade e segurança em meio ao caos urbano, é o espaço ideal para programas familiares e muita diversão. Dentro de suas construções em estilo brutalista, referência em arquitetura dos anos 60, o  Clube abriga instalações e equipamentos de última geração, oferecendo aos seus associados e dependentes, muito conforto e tranquilidade para a prática de suas atividades esportivas.O Clube Paineiras do Morumby oferece ainda inúmeras atividades socioculturais. Visando um melhor convívio social e familiar, além de tornar os dias dos associados ainda mais divertidos e culturais. Para atender ao seu quadro associativo, composto de aproximadamente 25 mil pessoas, o Clube Paineiras do Morumby conta com instalações de ponta e um moderno completo complexo esportivo, além das instalações destinadas aos eventos e atividades da área sociocultural do clube.  Seu complexo esportivo é composto por: 7 piscinas aquecidas, sendo uma olímpica, uma piscina kids e 5 piscinas sociais; 2 quadras de peteca, 4 quadras de areia, 15 quadras de tênis, 3 ginásios, pista de skate, pista de atletismo, 1 campo de futebol (medidas oficiais), 1 campo de society, fitness completo, sala de levantamento de peso olímpico, cancha de bocha, além de salas para atividades específicas como pilates, boxe, yoga, dentre outras.'
}, {
    "id":
    "2",
    "name":
    "Clube Libano",
    "release_year":
    "1972",
    "poster_url":
    "https://i.imgur.com/aFhJCip.jpg",
    "descrição":
    'Próximo ao Parque do Ibirapuera e ao bairro de Moema, o clube Monte Líbano foi fundado em 1934. O espaço conta com quadras, piscinas ao ar livre e cobertas, academia de ginástica e teatro.No clube, você pode aproveitar ainda a deliciosa gastronomia libanesa. São esfihas, quibes, charutos de repolho e outros pratos tradicionais do Líbano, como arroz de carneiro com coalhada fresca.'
}, {
    "id":
    "2",
    "name":
    "Clube Pinheiros",
    "release_year":
    "1972",
    "poster_url":
    "https://i.imgur.com/sGsw4oH.jpg",
    "descrição":
    'Um dos mais tradicionais clubes de SP é também um dos mais antigos. O Esporte Clube Pinheiros foi fundado em 1899 por um alemão com o nome de Sport Club Germania. O clube ocupa uma área de 170.000 m² na região central da capital paulista. Dono de uma excelente infraestrutura de esporte e lazer, o Pinheiros tem 24 quadras de tênis, seis piscinas, sete ginásios, três campos de futebol, atividades sociais e cinema, por exemplo. Há também no clube um trabalho de formação com mais de 3500 crianças de 3 a 14 anos. Não à toa, é também conhecido como o clube mais olímpico do país, pois já conquistou 12 medalhas olímpicas e 15 paralímpicas. Para fazer parte do clube, é preciso preencher uma proposta e candidatar-se. O Pinheiros não vende títulos sociais. A venda de títulos é feita por associados que desejem vender os seus, desde que a proposta do candidato esteja aprovada.'
}, {
    "id":
    "2",
    "name":
    "Clube Sirio",
    "release_year":
    "1972",
    "poster_url":
    "https://i.imgur.com/MxXbgMD.jpg",
    "descrição":
    'Se você procurar por clubes com piscina em SP, não pode faltar o Esporte Clube Sírio nesta lista. O clube foi fundado em 1917 por jovens imigrantes sírios e libaneses e teve sua primeira sede na rua do Comércio, no centro de São Paulo. O atual endereço, na Avenida Indianópolis, teve suas obras iniciadas em 1950. Para tornar-se sócio do Sírio, é preciso preencher um formulário com informações como estado civil e número de filhos. Consulte aqui. O Sírio atende os moradores de Moema, um dos bairros mais nobres de SP. A região tem o melhor IDH (Índice de Desenvolvimento Humano) de São Paulo e um dos maiores do Brasil. A qualidade de vida é comparável a países europeus como Dinamarca, Suécia e Noruega.'
}]
