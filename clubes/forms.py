from django.forms import ModelForm
from .models import Post, Comment



class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'descricao',
            'poster_url',
        ]
        labels = {
            'name': 'Nome',
            'descricao': 'Descrição',
            'poster_url': 'URL da Foto',
        }


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Resenha',
        }