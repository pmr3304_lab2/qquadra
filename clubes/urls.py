from django.urls import path

from . import views

app_name = 'clubes'
urlpatterns = [    
    path('search/', views.search_post, name='search'),
    #path('', views.list_post, name='index'),
    #path('create/', views.create_post, name='create'),
    #path('update/<int:clube_id>/', views.update_post, name='update'),
    #path('delete/<int:clube_id>/', views.delete_post, name='delete'),
    #path('<int:clube_id>/', views.detail_post, name='detail'),
    path('', views.PostListView.as_view(), name='index'),
    path('create/', views.PostCreateView.as_view(), name='create'),
    path('update/<int:pk>/', views.PostUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.PostDeleteView.as_view(), name='delete'),
    path('<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path('<int:clube_id>/comment/', views.create_comment, name='comment'),
    path('category/', views.CategoryListView.as_view(), name='category'),
    path('category/create', views.CategoryCreateView.as_view(), name='create-category'),
    path('category/detail/<int:category_id>', views.list_category_clubes, name='categorydetail'),
    
]
