from django.db import models
from django.conf import settings

class Post(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=True)
    descricao = models.CharField(max_length=255)
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name}'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    clube = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    descricao = models.CharField(max_length=255, default="Descrição da categoria")
    clubes = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'
