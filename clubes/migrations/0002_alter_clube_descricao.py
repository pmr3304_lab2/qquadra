# Generated by Django 3.2.7 on 2021-11-18 19:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clubes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clube',
            name='descricao',
            field=models.CharField(max_length=255),
        ),
    ]
