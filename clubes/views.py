from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .temp_data import clube_data
from .models import Post, Comment, Category
from django.views import generic
from .forms import PostForm, CommentForm



'''
def detail_post(request, clube_id):
    clube = get_object_or_404(Post, pk=clube_id)
    context = {'clube': clube}
    return render(request, 'clubes/detail.html', context)
'''
class PostDetailView(generic.DetailView):
    model = Post
    context_object_name = 'clube'
    template_name = 'clubes/detail.html'


'''
def list_post(request):
    clube_list = Post.objects.all()
    context = {'clube_list': clube_list}
    return render(request, 'clubes/index.html', context)
'''
class PostListView(generic.ListView):
    model = Post
    context_object_name = 'clube_list'
    template_name = 'clubes/index.html'



def search_post(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        clube_list = Post.objects.filter(name__icontains=search_term)
        context = {"clube_list": clube_list}
    return render(request, 'clubes/search.html', context)

'''
def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            clube_name = form.cleaned_data['name']
            clube_descricao = form.cleaned_data['descricao']
            clube_poster_url = form.cleaned_data['poster_url']
            clube = Post(name=clube_name,
                         descricao=clube_descricao,
                         poster_url=clube_poster_url)
            clube.save()
            return HttpResponseRedirect(
                reverse('clubes:detail', args=(clube.id, )))
    else:
        form = PostForm()
        context = {'form': form}
        return render(request, 'clubes/create.html', context)
'''
class PostCreateView(generic.CreateView):
    context_object_name = 'clube'
    form_class = PostForm
    model = Post
    template_class = PostForm
    template_name = "clubes/create.html"

    def get_success_url(self):
        return reverse('clubes:detail', kwargs={'pk': self.object.id})

'''
def update_post(request, clube_id):
    clube = get_object_or_404(Post, pk=clube_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            clube.name = form.cleaned_data['name']
            clube.descricao = form.cleaned_data['descricao']
            clube.poster_url = form.cleaned_data['poster_url']
            clube.save()
            return HttpResponseRedirect(
                reverse('clubes:detail', args=(clube.id, )))
    else:
        form = PostForm(
            initial={
                'name': clube.name,
                'descricao': clube.descricao,
                'poster_url': clube.poster_url
            })

    context = {'clube': clube, 'form': form}
    return render(request, 'clubes/update.html', context)
'''
class PostUpdateView(generic.UpdateView):
    context_object_name = 'clube'
    form_class = PostForm
    model = Post
    template_class = PostForm
    template_name = "clubes/update.html"

    def get_success_url(self):
        return reverse('clubes:detail', kwargs={'pk': self.object.id})


'''
def delete_post(request, clube_id):
    clube = get_object_or_404(Post, pk=clube_id)

    if request.method == "POST":
        clube.delete()
        return HttpResponseRedirect(reverse('clubes:index'))

    context = {'clube': clube}
    return render(request, 'clubes/delete.html', context)
'''

class PostDeleteView(generic.DeleteView):
    model = Post
    context_object_name = 'clube'
    template_name = 'clubes/delete.html'
    def get_success_url(self):
        return reverse('clubes:index')

def create_comment(request, clube_id):
    clube = get_object_or_404(Post, pk=clube_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            clube=clube)
            comment.save()
            return HttpResponseRedirect(
                reverse('clubes:detail', args=(clube_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'clube': clube}
    return render(request, 'clubes/comment.html', context)


class CategoryListView(generic.ListView):
    model = Category
    template_name = 'clubes/category.html'


class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'clubes/create-category.html'
    fields = ['name', 'author', 'clubes']
    success_url = reverse_lazy('clubes:category')


def list_category_clubes(request, category_id):
    category = get_object_or_404(Category, pk= category_id)
    context = {'category': category}
    return render(request, 'clubes/categorydetail.html', context)
